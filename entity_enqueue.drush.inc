<?php

/**
 * @file entity_enqueue.drush.inc
 *   Drush integration for Entity Enqueue.
 */

/**
 * Implementation of hook_drush_command().
 */
function entity_enqueue_drush_command() {
  $items = array();

  $data_option = array(
    'description' => dt('Arbitrary data to be associated with the new task in the queue, JSON-encoded.'),
    'example-value' => '{\'parent_nid\':517}',
    'value' => 'required',
  );

  $verify_option = array(
    'description' => dt('Check if queue exists before enqueueing (ask,yes,no).'),
    'example-value' => 'ask',
    'value' => 'required',
  );

  $items['entity-enqueue'] = array(
    'description' => dt('Enqueue an entity into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the entity.'),
      'entity_id' => dt('Entity IDs to enqueue. Use \'-\' to read EOL-delimited IDs from STDIN.'),
    ),
    'options' => array(
      'type' => array(
        'description' => dt('Optional. Specify entity type. Default is \'node\'.'),
        'example-value' => 'node,user,file,...',
        'value' => 'required',
      ),
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eeq'),
    'examples' => array(
      'drush entity-enqueue my-queue 1' => 'Enqueues entity ID 1 of default entity type (node) into \'my-queue\' queue',
      'drush entity-enqueue my-queue 1 2 3' => 'Enqueues entity IDs 1, 2 and 3 of default entity type (node) into \'my-queue\' queue',
      'drush entity-enqueue my-queue -' => 'Enqueues entity IDs read from STDIN of default entity type (node) into \'my-queue\' queue',
      'drush entity-enqueue my-queue 1 --type=user' => 'Enqueues user 1 into \'my-queue\' queue',
    ),
  );

  $items['entity-enqueue-file'] = array(
    'description' => dt('Enqueue a managed file into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the file.'),
      'fid' => dt('File IDs to enqueue. Use \'-\' to read EOL-delimited IDs from STDIN.'),
    ),
    'options' => array(
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eeqf'),
    'examples' => array(
      'drush entity-enqueue-file my-queue 1' => 'Enqueues File ID 1 into \'my-queue\' queue.',
    ),
  );

  $items['entity-enqueue-node'] = array(
    'description' => dt('Enqueue a node into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the node.'),
      'nid'  => dt('Node IDs to enqueue. Use \'-\' to read EOL-delimited IDs from STDIN.'),
    ),
    'options' => array(
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eeqn'),
    'examples' => array(
      'drush entity-enqueue-node my-queue 1' => 'Enqueues Node ID 1 into \'my-queue\' queue.',
    ),
  );

  $items['entity-enqueue-taxonomy-term'] = array(
    'description' => dt('Enqueue a taxonomy term into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the taxonomy term.'),
      'tid' => dt('Taxonomy term IDs to enqueue.'),
    ),
    'options' => array(
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eeqtt'),
    'examples' => array(
      'drush entity-enqueue-taxonomy-term my-queue 1' => 'Enqueues taxonomy term ID 1 into \'my-queue\' queue.',
    ),
  );

  $items['entity-enqueue-taxonomy-vocabulary'] = array(
    'description' => dt('Enqueue a taxonomy vocabulary into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the taxonomy vocabulary.'),
      'vid' => dt('Taxonomy vocabulary IDs to enqueue.'),
    ),
    'options' => array(
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eeqtv'),
    'examples' => array(
      'drush entity-enqueue-taxonomy-vocabulary my-queue 1' => 'Enqueues taxonomy vocabulary ID 1 into \'my-queue\' queue.',
    ),
  );

  $items['entity-enqueue-user'] = array(
    'description' => dt('Enqueue a user into a Drupal queue.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'queue_name' => dt('Queue name where to enqueue the user.'),
      'uid' => dt('User IDs to enqueue.'),
    ),
    'options' => array(
      'data' => $data_option,
      'verify-queue-name' => $verify_option,
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('eequ'),
    'examples' => array(
      'drush entity-enqueue-user my-queue 1' => 'Enqueues User ID 1 into \'my-queue\' queue.',
    ),
  );

  return $items;
}

function drush_entity_enqueue($queue_name, $entity_id) {
  $entity_type = drush_get_option('type', 'node');

  _drush_entity_enqueue($queue_name, $entity_id, $entity_type);
}

function drush_entity_enqueue_file($queue_name, $fid) {
  _drush_entity_enqueue($queue_name, $fid, 'file');
}

function drush_entity_enqueue_node($queue_name, $nid) {
  _drush_entity_enqueue($queue_name, $nid, 'node');
}

function drush_entity_enqueue_taxonomy_term($queue_name, $tid) {
  _drush_entity_enqueue($queue_name, $tid, 'taxonomy_term');
}

function drush_entity_enqueue_taxonomy_vocabulary($queue_name, $vid) {
  _drush_entity_enqueue($queue_name, $vid, 'taxonomy_vocabulary');
}

function drush_entity_enqueue_user($queue_name, $uid) {
  _drush_entity_enqueue($queue_name, $uid, 'user');
}

/**
 * Command argument complete callback. Provides argument
 * values for shell completion.
 *
 * @return
 *  Array of popular fillings.
 */
function entity_enqueue_entity_enqueue_complete() {
  // @todo: Return queue names autocomplete only for first arg, if possible.
  drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  $queues = module_invoke_all('cron_queue_info');
  drupal_alter('cron_queue_info', $queues);
  return array(
    'values' => array_keys($queues),
  );
}

function entity_enqueue_entity_enqueue_file_complete() {
  return entity_enqueue_entity_enqueue_complete();
}

function entity_enqueue_entity_enqueue_taxonomy_term_complete() {
  return entity_enqueue_entity_enqueue_complete();
}

function entity_enqueue_entity_enqueue_taxonomy_vocabulary_complete() {
  return entity_enqueue_entity_enqueue_complete();
}

function entity_enqueue_entity_enqueue_node_complete() {
  return entity_enqueue_entity_enqueue_complete();
}

function entity_enqueue_entity_enqueue_user_complete() {
  return entity_enqueue_entity_enqueue_complete();
}

function _drush_entity_enqueue($queue_name, $entity_id, $entity_type) {
  $data = _drush_entity_enqueue_get_data_option();

  // Shift command name
  drush_shift();

  // Shift queue name
  $arg = drush_shift();
  if (is_numeric($arg)) {
    $msg = 'The queue name argument looks like an entity number. ';
    $msg .= 'Perhaps you forgot to specify it. Continue anyway?';
    if (!drush_confirm(dt($msg))) {
      return drush_user_abort();
    }
  }

  $verify_queue_name = strtolower(drush_get_option('verify-queue-name', 'ask'));
  if ($verify_queue_name !== 'no') {
    $queues = module_invoke_all('cron_queue_info');
    drupal_alter('cron_queue_info', $queues);
    if (!array_key_exists($arg, $queues)) {
      $error_message = dt('The queue \'@queue\' does not exists.',
        array( '@queue' => $arg ));
      if ($verify_queue_name === 'yes') {
        return drush_set_error('DRUSH_EEQ_QUEUE_NOT_FOUND', $error_message);
      }
      if (!drush_confirm($error_message . ' ' . 'Continue anyway?')) {
        return drush_set_error('DRUSH_EEQ_QUEUE_NOT_FOUND', $error_message);
      }
    }
  }

  if ($entity_id == '-') {
    drush_log(dt('Reading entity IDs from STDIN.'));
    $entity_id_array = explode(PHP_EOL, stream_get_contents(STDIN));
    drush_shift();  // Shift '-' to allow following command line IDs
  }

  while($entity_id = drush_shift()) {
    $entity_id_array[] = $entity_id;
  }

  foreach($entity_id_array as $id) {
    $id = trim($id);
    // Skip empty lines
    if (empty($id)) {
      continue;
    }
    entity_enqueue_entity($queue_name, $id, $entity_type, $data);
  }
}

function _drush_entity_enqueue_get_data_option() {
  $data = drush_get_option('data', array());

  if (!empty($data)) {
    drush_log('Data (string): ' . print_r($data, TRUE), 'notice');

    $data = drush_json_decode($data, TRUE);
    drush_log('Data (decoded):' . PHP_EOL . trim(print_r($data, TRUE)), 'notice');
  }
  return $data;
}
